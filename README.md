# README #

## Initial Setup

Run `./setup.sh`, which will install all necessary components for the project.

## Running the Project

Run `./run.sh`, which will compile and minify the less file and run the express server.

Then, visit [localhost:8080](http://localhost:8080) in your browser to view the site.

## About this Project

Simple static site with less style capability running on a very basic Express server.

Project built by [Jonathan Fann](http://jfann.com) in September, 2017.