var express = require('express'),
    app = express()

app.use(express.static('static'))

app.listen(8080, function () {
  console.log('running at http://localhost:8080')
})
