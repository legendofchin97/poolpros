$(document).ready(function() {
    /* parse local json for results to emulate API get response
     * TODO: hook up filters if time permits
     */

     var doModal = function(name) {
         $('body').addClass('modal-open');
         $('#modal h2.company-name').html(name);
         $('#modal span.company-name').html(name);
         $('#modal').fadeIn();
         $('.close').click(function() {
             $('#modal').fadeOut();
             $('body').removeClass('modal-open');
         });
         $('#modal input.required').blur(function() {
             if( !$(this).val() ) {
                 $(this).parents().removeClass('valid')
             } else {
                 $(this).parents().addClass('valid');
             }
         });

         $('#modal').mousedown(function(e) {
            /* check where click is to close if click overlay */
            var clicked = $(e.target);
                if (clicked.is('.modal-content') || clicked.parents().is('.modal-content')) {
                    /* provide x to click too and allow submit to close */
                    return;
                } else {
                    $('#modal').fadeOut();
                    $('body').removeClass('modal-open')
                }
            });
     }

     $.getJSON('data/dealers.json', function(data) {
         $.each(data.dealers, function (index, dealer) {
             certsHtml = '';
             if (dealer.data.weekHours.sun == '') {
                 dealer.data.weekHours.sun = 'CLOSED';
             }
             if (dealer.data.certifications) {
                 var certs = dealer.data.certifications;
                 $.each(certs, function(i, cert) {
                     if (cert == 'Installation Pro') {
                         fa = 'fa-star';
                     } else if (cert == 'Commercial Pro') {
                         fa = 'fa-group';
                     } else if (cert == 'Residential Pro') {
                         fa = 'fa-home';
                     } else {
                         fa = 'fa-cog';
                     }
                     certsHtml += ('<span class="cert ' + cert.replace(' ', '') + '"><i class="fa ' + fa + '"></i>' + cert + '</span>');
                 });
             }
             dealer.data.html =
             '<div class="card"><div class="card-inner">\n' +
                '<h2>' + dealer.data.name + '</h2>\n' +
                '<hr />' +
                '<div class="phone">\n' +
                '<span class="fa-stack fa-lg">' +
                '<i class="fa fa-circle fa-stack-2x"></i>' +
                '<i class="fa fa-phone fa-stack-1x fa-inverse"></i>' +
                '</span>' +
                '<strong>' + dealer.data.phone1 + '</strong>\n' +
                '</div>' +
                '<p><small>Can\'t talk now? Click below to send an email.</small></p>\n' +
                '<button class="contact-button" id="' + dealer.data.name.replace(' ', '-') + '"><i class="fa fa-envelope"></i> Contact this Pro</button>\n' +
                '<div class="hours">\n' +
                '<p><strong>Business Hours</strong></p>' +
                '<p>Weekdays ' + dealer.data.weekHours.mon + '</p>\n' +
                '<p>Saturdays ' + dealer.data.weekHours.sat + '</p>\n' +
                '<p>Sundays ' + dealer.data.weekHours.sun + '</p>\n' +
                '</div></div>' +
                '<div class="certifications">' + certsHtml + '</div>\n' +
            '</div>';
            $( '<div />', {
                'class': 'one-third',
                'id': dealer.data.companyID,
                html: dealer.data.html
            }).appendTo( "#results" );
        });
        $('.contact-button').click(function() {
            identificationPlease = $(this).attr('id').replace('-', ' ');
            doModal(identificationPlease);
        });
    });

    /* nav stuff */
    var doNav = function() {
        var navSpan = $('#nav-right .nav-items span');
        navSpan.click(function(){
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#dropdown' + $(this).attr('id')).hide();
            } else if (navSpan.hasClass('active')){
                navSpan.removeClass('active');
                $('.dropdown').hide();
                $(this).addClass('active');
                $('#dropdown' + $(this).attr('id')).show();
            } else {
                $(this).addClass('active');
                $('#dropdown' + $(this).attr('id')).show();
            }
        });
    }
    doNav();

    /* mobile nav stuff */
    $('#mobile-menu').click(function() {
        $('#mobile-menu-overlay').fadeIn();
        $('#mobile-menu-overlay a').click(function() {
            /* since there's no other pages, pretend like we went somewhere */
            $('#mobile-menu-overlay').hide();
        });
        $('#mobile-menu-overlay .fa-close').click(function() {
            $('#mobile-menu-overlay').fadeOut();
        })
    });
});
