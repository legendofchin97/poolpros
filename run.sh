#!/bin/bash
echo 'compiling/minifying less'
lessc --clean-css static/styles/custom.less static/styles/custom.min.css
echo 'starting server'
node server.js
